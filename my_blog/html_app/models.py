from django.db import models
from PIL import Image

# Create your models here.


class ResolutionErrorException(Exception):
    pass


class HtmlBlocks(models.Model):

    name_title = models.CharField(max_length=120, verbose_name="Заглавие")
    name_title_html = models.TextField(verbose_name='Заглавие c HTML', null=True, blank=True)
    description_1 = models.TextField(verbose_name='Текстовый блок №1', null=True, blank=True)
    description_2 = models.TextField(verbose_name='Текстовый блок №2', null=True, blank=True)
    image_1 = models.ImageField(upload_to='images/', verbose_name='Изображение №1', null=True, blank=True)
    image_2 = models.ImageField(upload_to='images/', verbose_name='Изображение №2', null=True, blank=True)
    image_3 = models.ImageField(upload_to='images/', verbose_name='Изображение №3', null=True, blank=True)
    video_img = models.ImageField(upload_to='images/', null=True, blank=True, verbose_name="Фоновое изоражение видео")
    video_url = models.CharField(max_length=255, verbose_name='Ссылка на видео', null=True, blank=True)
    article_url = models.CharField(max_length=255, verbose_name='Ссылка на статью', null=True, blank=True)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.name_title

    class Meta:
        verbose_name = "HTML блок"
        verbose_name_plural = "HTML блоки"


class BigSlider(models.Model):

    VALID_RESOLUTION = (720, 1920)
    MAX_IMAGE_SIZE = 3670016

    name_title = models.CharField(max_length=120, verbose_name="Заглавие банера")
    name_title_html = models.TextField(verbose_name='Заглавие c HTML', null=True, blank=True)
    description = models.TextField(verbose_name='Текстовый блок', null=True, blank=True)
    image = models.ImageField(upload_to='images/', verbose_name='Изображение')
    article_url = models.CharField(max_length=255, verbose_name='Ссылка на статью', null=True, blank=True)
    page = models.CharField(max_length=255, verbose_name='Страница отображения')

    def __str__(self):
        return self.name_title

    class Meta:
        verbose_name = "Слайдер"
        verbose_name_plural = "Слайдеры"

    def save(self, *args, **kwargs):
        image = self.image
        img = Image.open(image)
        valid_height, valid_width = self.VALID_RESOLUTION
        if img.height != valid_height or img.width != valid_width:
            raise ResolutionErrorException('Загруженное изображение не соответствует требуемым размерам!')
        super().save(*args, **kwargs)
