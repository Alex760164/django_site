from django import forms
from django.forms import ModelForm, ValidationError
from django.utils.safestring import mark_safe
from django.contrib import admin
from .models import *
from ckeditor_uploader.widgets import CKEditorUploadingWidget


class BigSliderAdminForm(ModelForm):

    VALID_RESOLUTION = (720, 1920)
    name_title_html = forms.CharField(label="Заглавие c HTML", widget=CKEditorUploadingWidget(), required=False)
    description = forms.CharField(label="Текстовый блок", widget=CKEditorUploadingWidget(), required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['image'].help_text = mark_safe(
            '<span style="color:blue; font-size:13px;">'
            'Загружаемое изображение должно иметь разрешение {}рх на {}рх'
            '</span>'.format(
                *BigSlider.VALID_RESOLUTION
            )
        )

    def clean_image(self):
        image = self.cleaned_data['image']
        img = Image.open(image)
        valid_height, valid_width = BigSlider.VALID_RESOLUTION
        if image.size > BigSlider.MAX_IMAGE_SIZE:
            raise ValidationError('Размер загружаемого изображения не должен превышать 3.5МБ!')
        if img.height != valid_height or img.width != valid_width:
            raise ValidationError('Загруженное изображение не соответствует требуемым размерам!')
        return image

    class Meta:
        model = BigSlider
        fields = '__all__'


class HtmlBlocksAdminForm(ModelForm):
    name_title_html = forms.CharField(label="Заглавие c HTML", widget=CKEditorUploadingWidget(), required=False)
    description_1 = forms.CharField(label="Текстовый блок №1", widget=CKEditorUploadingWidget(), required=False)
    description_2 = forms.CharField(label="Текстовый блок №2", widget=CKEditorUploadingWidget(), required=False)

    class Meta:
        model = HtmlBlocks
        fields = '__all__'


class BigSliderAdmin(admin.ModelAdmin):

    form = BigSliderAdminForm

    list_display = ("name_title", "get_image", "get_description", "article_url", "page", )
    readonly_fields = ("get_image_form",)
    fieldsets = (
        (None, {
            "fields": ("name_title",)
        }),
        (None, {
            "fields": ("name_title_html",)
        }),
        (None, {
            "fields": ("description",)
        }),
        (None, {
            "fields": (("image", "get_image_form"),)
        }),
        (None, {
            "fields": ("article_url",)
        }),
        (None, {
            "fields": ("page",)
        }),
    )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_image(self, obj):
        if obj.image:
            return mark_safe(f'<img src={obj.image.url} width="50" height="25">')
        else:
            pass

    def get_image_form(self, obj):
        if obj.image:
            return mark_safe(f'<img src={obj.image.url} width="100" height="50">')
        else:
            pass

    def get_description(self, obj):
        if obj.description:
            if len(obj.description) > 100:
                return obj.description[:100] + '...'
            else:
                return obj.description
        else:
            pass

    get_description.short_description = "Описание"
    get_image.short_description = "Изображение"
    get_image_form.short_description = ""


class HtmlBlocksAdmin(admin.ModelAdmin):

    form = HtmlBlocksAdminForm

    list_display = ("name_title",
                    "get_description_1",
                    "get_description_2",
                    "get_image_1",
                    "get_image_2",
                    "get_image_3",
                    "get_video_img",
                    "get_video_url",
                    "article_url",
                    "slug",)

    readonly_fields = ("get_image_1_form",
                       "get_image_2_form",
                       "get_image_3_form",
                       "get_video_img_form",
                       )
    fieldsets = (
        (None, {
            "fields": ("name_title",)
        }),
        (None, {
            "fields": ("name_title_html",)
        }),
        (None, {
            "fields": ("description_1",)
        }),
        (None, {
            "fields": ("description_2",)
        }),
        (None, {
            "fields": (("image_1", "get_image_1_form"),)
        }),
        (None, {
            "fields": (("image_2", "get_image_2_form"),)
        }),
        (None, {
            "fields": (("image_3", "get_image_3_form"),)
        }),
        (None, {
            "fields": (("video_img", "get_video_img_form"),)
        }),
        (None, {
            "fields": ("video_url",)
        }),
        (None, {
            "fields": ("article_url",)
        }),
        (None, {
            "fields": ("slug",)
        }),
    )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_image_1(self, obj):
        if obj.image_1:
            return mark_safe(f'<img src={obj.image_1.url} width="50" height="50">')
        else:
            pass

    def get_image_2(self, obj):
        if obj.image_2:
            return mark_safe(f'<img src={obj.image_2.url} width="50" height="50">')
        else:
            pass

    def get_image_3(self, obj):
        if obj.image_3:
            return mark_safe(f'<img src={obj.image_3.url} width="50" height="50">')
        else:
            pass

    def get_video_img(self, obj):
        if obj.video_img:
            return mark_safe(f'<img src={obj.video_img.url} width="50" height="50">')
        else:
            pass

    def get_image_1_form(self, obj):
        if obj.image_1:
            return mark_safe(f'<img src={obj.image_1.url} width="90" height="90">')
        else:
            pass

    def get_image_2_form(self, obj):
        if obj.image_2:
            return mark_safe(f'<img src={obj.image_2.url} width="90" height="90">')
        else:
            pass

    def get_image_3_form(self, obj):
        if obj.image_3:
            return mark_safe(f'<img src={obj.image_3.url} width="90" height="90">')
        else:
            pass

    def get_video_img_form(self, obj):
        if obj.video_img:
            return mark_safe(f'<img src={obj.video_img.url} width="90" height="90">')
        else:
            pass

    def get_video_url(self, obj):
        if obj.video_url:
            return mark_safe(f'<a href="{obj.video_url}" alt="Перейти" title="{obj.video_url}" target="_blank">Ссылка</a>')
        else:
            pass

    def get_description_1(self, obj):
        if obj.description_1:
            if len(obj.description_1) > 50:
                return obj.description_1[:50] + '...'
            else:
                return obj.description_1
        else:
            pass

    def get_description_2(self, obj):
        if obj.description_2:
            if len(obj.description_2) > 50:
                return obj.description_2[:50] + '...'
            else:
                return obj.description_2
        else:
            pass

    get_description_1.short_description = "Текстовый блок №1"
    get_description_2.short_description = "Текстовый блок №2"
    get_image_1.short_description = "Изображение №1"
    get_image_2.short_description = "Изображение №2"
    get_image_3.short_description = "Изображение №3"
    get_video_img.short_description = "Фоновое изоражение видео"
    get_video_url.short_description = "Ссылка на видео"
    get_image_1_form.short_description = ""
    get_image_2_form.short_description = ""
    get_image_3_form.short_description = ""
    get_video_img_form.short_description = ""


# Register your models here.
admin.site.register(HtmlBlocks, HtmlBlocksAdmin)
admin.site.register(BigSlider, BigSliderAdmin)
