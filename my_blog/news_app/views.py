from django.shortcuts import render
from django.views.generic import DetailView
from django.conf import settings
import os
from .models import Articles
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger    # импорт для пагинации


# Create your views here.


def news(request):
    article_list = Articles.objects.all().order_by('-date')
    # Пагинация начало
    paginator = Paginator(article_list, 2)  # 2 поста на каждой странице
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        # Если страница не является целым числом, поставим первую страницу
        articles = paginator.page(1)
    except EmptyPage:
        # Если страница больше максимальной, доставить последнюю страницу результатов
        articles = paginator.page(paginator.num_pages)
    # Пагинация конец

    file_path = os.path.join(settings.BASE_DIR, 'news_app/static/news_app/includes/news_page.txt')
    with open(file_path, "r", encoding="utf-8") as f:
        lines = f.readlines()
    data_text = {}
    for i in lines:
        current_data = i.strip().split('=')
        data_text[current_data[0].replace(' ', '')] = current_data[1]
    data = {
        'articles': articles,
        'data_text': data_text,
    }
    return render(request, 'news_app/articles_page.html', data)


class DetailArticlesPageView(DetailView):
    model = Articles

    def get_context_data(self, **kwargs):
        context = super(DetailArticlesPageView, self).get_context_data(**kwargs)
        file_path = os.path.join(settings.BASE_DIR, 'news_app/static/news_app/includes/news_page.txt')
        with open(file_path, "r", encoding="utf-8") as f:
            lines = f.readlines()
        data_text = {}
        for i in lines:
            current_data = i.strip().split('=')
            data_text[current_data[0].replace(' ', '')] = current_data[1]
        context['data_text'] = data_text
        return context

    template_name = 'news_app/article_page.html'
