from django.urls import path
from . import views
from .views import DetailArticlesPageView


urlpatterns = [
    path('', views.news, name='news'),
    path('<slug:slug>/', DetailArticlesPageView.as_view(), name='employee'),
]
