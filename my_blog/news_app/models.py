from django.db import models
from PIL import Image

# Create your models here.


class ResolutionErrorException(Exception):
    pass


class Articles(models.Model):

    VALID_RESOLUTION = (485, 770)
    MAX_IMAGE_SIZE = 3670016

    title = models.CharField(max_length=120, verbose_name='Название статьи')
    text = models.TextField(null=True, verbose_name='Текст статьи')
    date = models.DateTimeField(verbose_name='Дата публикации')
    image = models.ImageField(upload_to='images/', verbose_name='Изображение', null=True)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Статья"
        verbose_name_plural = "Статьи"

    def save(self, *args, **kwargs):
        image = self.image
        img = Image.open(image)
        valid_height, valid_width = self.VALID_RESOLUTION
        if img.height != valid_height or img.width != valid_width:
            raise ResolutionErrorException('Загруженное изображение не соответствует требуемым размерам!')
        super().save(*args, **kwargs)
