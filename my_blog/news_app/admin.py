from django import forms
from django.forms import ModelForm, ValidationError
from django.utils.safestring import mark_safe
from django.contrib import admin
from .models import *
from ckeditor_uploader.widgets import CKEditorUploadingWidget


class ArticlesAdminForm(forms.ModelForm):

    VALID_RESOLUTION = (485, 770)
    text = forms.CharField(label="Текст статьи", widget=CKEditorUploadingWidget())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['image'].help_text = mark_safe(
            '<span style="color:blue; font-size:13px;">'
            'Загружаемое изображение должно иметь разрешение {}рх на {}рх'
            '</span>'.format(
                *Articles.VALID_RESOLUTION
            )
        )

    def clean_image(self):
        image = self.cleaned_data['image']
        img = Image.open(image)
        valid_height, valid_width = Articles.VALID_RESOLUTION
        if image.size > Articles.MAX_IMAGE_SIZE:
            raise ValidationError('Размер загружаемого изображения не должен превышать 3.5МБ!')
        if img.height != valid_height or img.width != valid_width:
            raise ValidationError('Загруженное изображение не соответствует требуемым размерам!')
        return image

    class Meta:
        model = Articles
        fields = '__all__'


class ArticlesAdmin(admin.ModelAdmin):

    form = ArticlesAdminForm

    list_display = ("title", "get_image", "get_description", "slug", "date")
    readonly_fields = ("get_image_form",)
    fieldsets = (
        (None, {
            "fields": ("title",)
        }),
        (None, {
            "fields": ("text",)
        }),
        (None, {
            "fields": ("date",)
        }),
        (None, {
            "fields": (("image", "get_image_form"),)
        }),
        (None, {
            "fields": ("slug",)
        }),
    )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_image(self, obj):
        if obj.image:
            return mark_safe(f'<img src={obj.image.url} width="50" height="50">')
        else:
            pass

    def get_image_form(self, obj):
        if obj.image:
            return mark_safe(f'<img src={obj.image.url} width="90" height="90">')
        else:
            pass

    def get_description(self, obj):
        if obj.text:
            if len(obj.text) > 100:
                return obj.text[:100] + '...'
            else:
                return obj.text
        else:
            pass

    get_image.short_description = "Изображение"
    get_image_form.short_description = ""
    get_description.short_description = "Описание"


# Register your models here.
admin.site.register(Articles, ArticlesAdmin)

