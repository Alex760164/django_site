from django import forms
from django.forms import ModelForm, ValidationError
from django.utils.safestring import mark_safe
from django.contrib import admin
from .models import *
from ckeditor_uploader.widgets import CKEditorUploadingWidget


class StaffAdminForm(forms.ModelForm):

    VALID_RESOLUTION = (370, 370)
    description = forms.CharField(label="Описание", widget=CKEditorUploadingWidget())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['image'].help_text = mark_safe(
            '<span style="color:blue; font-size:13px;">'
            'Загружаемое изображение должно иметь разрешение {}рх на {}рх'
            '</span>'.format(
                *Staff.VALID_RESOLUTION
            )
        )

    def clean_image(self):
        image = self.cleaned_data['image']
        img = Image.open(image)
        valid_height, valid_width = Staff.VALID_RESOLUTION
        if image.size > Staff.MAX_IMAGE_SIZE:
            raise ValidationError('Размер загружаемого изображения не должен превышать 3.5МБ!')
        if img.height != valid_height or img.width != valid_width:
            raise ValidationError('Загруженное изображение не соответствует требуемым размерам!')
        return image

    class Meta:
        model = Staff
        fields = '__all__'


class StaffAdmin(admin.ModelAdmin):

    form = StaffAdminForm

    list_display = ("name", "get_image", "name_position", "slug", "get_description")
    readonly_fields = ("get_image_form",)
    fieldsets = (
        (None, {
            "fields": ("name",)
        }),
        (None, {
            "fields": ("name_position",)
        }),
        (None, {
            "fields": (("image", "get_image_form"),)
        }),
        (None, {
            "fields": ("description",)
        }),
        (None, {
            "fields": ("slug",)
        }),
    )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_image(self, obj):
        if obj.image:
            return mark_safe(f'<img src={obj.image.url} width="50" height="50">')
        else:
            pass

    def get_image_form(self, obj):
        if obj.image:
            return mark_safe(f'<img src={obj.image.url} width="90" height="90">')
        else:
            pass

    def get_description(self, obj):
        if obj.description:
            if len(obj.description) > 100:
                return obj.description[:100] + '...'
            else:
                return obj.description
        else:
            pass

    get_description.short_description = "Описание"
    get_image.short_description = "Изображение"
    get_image_form.short_description = ""


class MessagesAdmin(admin.ModelAdmin):

    list_display = ("second_name", "name", "telephone", "email", "message")
    fieldsets = (
        (None, {
            "fields": ("name",)
        }),
        (None, {
            "fields": ("second_name",)
        }),
        (None, {
            "fields": ("telephone",)
        }),
        (None, {
            "fields": ("email",)
        }),
        (None, {
            "fields": ("message",)
        }),
    )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


# Register your models here.
admin.site.register(Staff, StaffAdmin)
admin.site.register(Messages, MessagesAdmin)
