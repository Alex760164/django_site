from .models import Messages
from django.forms import ModelForm, TextInput, Textarea


class MessagesForm(ModelForm):
    class Meta:
        model = Messages
        fields = ['name', 'second_name', 'telephone', 'email', 'message']

        widgets = {
            "name": TextInput(attrs={
                'class': 'form-input',
                'id': 'contact-name',
                'type': 'text',
                'required': 'required',
            }),
            "second_name": TextInput(attrs={
                'class': 'form-input',
                'id': 'contact-sec-name',
                'type': 'text',
                'required': 'required',
            }),
            "telephone": TextInput(attrs={
                'class': 'form-input',
                'id': 'contact-phone',
                'type': 'text',
                'required': 'required',
            }),
            "email": TextInput(attrs={
                'class': 'form-input',
                'id': 'contact-email',
                'type': 'email',
                'required': 'required',
            }),
            "message": Textarea(attrs={
                'class': 'form-input',
                'id': 'contact-message',
                'required': 'required',
            }),
        }
