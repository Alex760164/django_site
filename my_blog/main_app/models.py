from django.db import models
from PIL import Image

# Create your models here.


class ResolutionErrorException(Exception):
    pass


class Staff(models.Model):

    VALID_RESOLUTION = (370, 370)
    MAX_IMAGE_SIZE = 3670016

    name = models.CharField(max_length=120, verbose_name="Имя сотрудника")
    name_position = models.CharField(max_length=120, verbose_name="Направление в работе")
    slug = models.SlugField(unique=True)
    image = models.ImageField(upload_to='images/', verbose_name='Изображение')
    description = models.TextField(verbose_name='Описание', null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Сотрудник"
        verbose_name_plural = "Сотрудники"

    def save(self, *args, **kwargs):
        image = self.image
        img = Image.open(image)
        valid_height, valid_width = self.VALID_RESOLUTION
        if img.height != valid_height or img.width != valid_width:
            raise ResolutionErrorException('Загруженное изображение не соответствует требуемым размерам!')
        super().save(*args, **kwargs)


class Messages(models.Model):

    name = models.CharField(max_length=120, verbose_name="Имя")
    second_name = models.CharField(max_length=120, verbose_name="Фамилия")
    telephone = models.CharField(max_length=20, verbose_name="Телефон")
    email = models.CharField(max_length=50, verbose_name="E-mail")
    message = models.TextField(verbose_name='Сообщение', null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"
