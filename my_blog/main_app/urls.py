from django.urls import path
from . import views
from .views import DetailStaffPageView


urlpatterns = [
    path('', views.index, name='index'),
    path('contact/', views.contact, name='contact'),
    path('about-us/', views.about_us, name='about_us'),
    path('staff/', views.staff, name='staff'),
    path('staff/<slug:slug>/', DetailStaffPageView.as_view(), name='employee'),
]
