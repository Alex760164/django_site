from django.shortcuts import render
from django.views.generic import DetailView
from django.conf import settings
import os
from .models import Staff
from html_app.models import HtmlBlocks, BigSlider
from .forms import MessagesForm
from django.contrib import messages


def index(request):

    query_our_team = Staff.objects.all().order_by('name')
    query_choose_us = HtmlBlocks.objects.get(slug="why_our_clients_choose_us")
    query_beauty_services = HtmlBlocks.objects.get(slug="a_variety_of_beauty_services")
    query_beauty_salon = HtmlBlocks.objects.get(slug="our_beauty_salon")
    query_big_slider = BigSlider.objects.filter(page="home")

    if query_our_team is not None:
        our_team = query_our_team
    else:
        our_team = ''

    if query_choose_us is not None:
        choose_us = query_choose_us
    else:
        choose_us = ''

    if query_beauty_services is not None:
        beauty_services = query_beauty_services
    else:
        beauty_services = ''

    if query_beauty_salon is not None:
        beauty_salon = query_beauty_salon
    else:
        beauty_salon = ''

    if query_big_slider is not None:
        big_slider = query_big_slider
    else:
        big_slider = ''

    file_path = os.path.join(settings.BASE_DIR, 'main_app/static/main_app/includes/base_page.txt')
    with open(file_path, "r", encoding="utf-8") as f:
        lines = f.readlines()
    data_text = {}
    for i in lines:
        current_data = i.strip().split('=')
        data_text[current_data[0].replace(' ', '')] = current_data[1]
    data = {
        'our_team': our_team,
        'data_text': data_text,
        'choose_us': choose_us,
        'beauty_services': beauty_services,
        'beauty_salon': beauty_salon,
        'big_slider': big_slider,
    }
    return render(request, 'main_app/home_page.html', data)


def contact(request):
    # Запись с формы в базу
    if request.method == 'POST':
        form = MessagesForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Сообщение успешно отправлено. Мы с вами в скором времени свяжимся')
        else:
            messages.error(request, 'ОШИБКА. Сообщение не отправлено. Попробуйте еще раз')

    # Вывод страницы Контакты
    form = MessagesForm()
    file_path = os.path.join(settings.BASE_DIR, 'main_app/static/main_app/includes/contact_page.txt')
    with open(file_path, "r", encoding="utf-8") as f:
        lines = f.readlines()
    data_text = {}
    for i in lines:
        current_data = i.strip().split('=')
        data_text[current_data[0].replace(' ', '')] = current_data[1]
    data = {
        'form': form,
        'data_text': data_text,
    }
    return render(request, 'main_app/contact_page.html', data)


def about_us(request):
    file_path = os.path.join(settings.BASE_DIR, 'main_app/static/main_app/includes/about_us_page.txt')
    with open(file_path, "r", encoding="utf-8") as f:
        lines = f.readlines()
    data_text = {}
    for i in lines:
        current_data = i.strip().split('=')
        data_text[current_data[0].replace(' ', '')] = current_data[1]

    query_who_we_are = HtmlBlocks.objects.get(slug="who_we_are")
    query_our_team = Staff.objects.all().order_by('name')
    query_video_presentation = HtmlBlocks.objects.get(slug="work_staff_video")

    if query_who_we_are is not None:
        who_we_are = query_who_we_are
    else:
        who_we_are = ''

    if query_our_team is not None:
        our_team = query_our_team
    else:
        our_team = ''

    if query_video_presentation is not None:
        video_presentation = query_video_presentation
    else:
        video_presentation = ''

    data = {
        'data_text': data_text,
        'who_we_are': who_we_are,
        'video_presentation': video_presentation,
        'our_team': our_team,
    }

    return render(request, 'main_app/about_us_page.html', data)


def staff(request):
    our_team = Staff.objects.all().order_by('name')
    file_path = os.path.join(settings.BASE_DIR, 'main_app/static/main_app/includes/staff_page.txt')
    with open(file_path, "r", encoding="utf-8") as f:
        lines = f.readlines()
    data_text = {}
    for i in lines:
        current_data = i.strip().split('=')
        data_text[current_data[0].replace(' ', '')] = current_data[1]
    data = {
        'our_team': our_team,
        'data_text': data_text,
    }
    return render(request, 'main_app/staff_page.html', data)


class DetailStaffPageView(DetailView):
    model = Staff

    def get_context_data(self, **kwargs):
        context = super(DetailStaffPageView, self).get_context_data(**kwargs)
        file_path = os.path.join(settings.BASE_DIR, 'main_app/static/main_app/includes/employee_page.txt')
        with open(file_path, "r", encoding="utf-8") as f:
            lines = f.readlines()
        data_text = {}
        for i in lines:
            current_data = i.strip().split('=')
            data_text[current_data[0].replace(' ', '')] = current_data[1]
        context['data_text'] = data_text
        return context

    template_name = 'main_app/employee_page.html'
