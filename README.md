### Для запуска приложения на новом компьютере нужно выполнить следующее:
#### 1.Склонировать репозиторий в рабочую папку, выполнив команду 
```git clone https://gitlab.com/Alex760164/django_site.git```
#### 2.В папке проекта создаем виртуальное окружение с помощью команды
```virtualenv virtualenv_name```
###### *virtualenv_name* - имя виртуального окружения
#### 3.Инициализируем виртуальное окружение
```source virtualenv_name/bin/activate```
###### или
```python3 -m venv virtualenv_name```
#### 4.Устанавливаем недостающие пакеты согласно списку пакетов из файла requirements.txt
###### Проверяем список установленных пакетов с помощью команды 
```pip list```
###### Для установки недостающих пакета(ов) используем команду
```pip install package_name``` 
###### *"package_name"* - имя пакета
###### Если есть необходимость установить весь перечень библиотек из файла *requirements.txt* в текущее окружение, то нужно выполнить следующую команду
```pip install -r requirements.txt```
#### 5.Создаем новые миграций на основе изменений в моделях
###### Под Windows
```python manage.py makemigritions```
###### Под Linux
```./manage.py makemigritions```
#### 6.Применяем миграций
###### Под Windows
```python manage.py migrate```
###### Под Linux
```./manage.py migrate```
#### 7.Запуск приложения
###### Под Windows
```python manage.py runserver```
###### Под Linux
```./manage.py runserver```